#To edit and compare internal_properties, use WINDEV integrated tools.
#Internal properties refer to the properties of controls in windows, reports, etc.
info :
 name : tuActivityApi_add_activity
 major_version : 26
 minor_version : 0
 type : 4
 description : ""
 subtype : 0
class :
 identifier : 0x117ceef80753905f
 internal_properties : BgAAAAYAAAB2/vstTMCJbS/hlxjFxirSqKvCUuv8YxgpWyl7S3iA
 code_elements :
  type_code : 10
  p_codes :
   -
     code : |1-
      tuActivityApi_add_activity is a Class
      	inherits cTestCase
      private
      	uuid_generator is cUuidGeneratorForTest dynamic
      	activity_api is cActivityApi dynamic
      	activity_repository is cActivityRepositoryForTest dynamic
      end
     type : 131072
  procedures :
   -
     name : Constructor
     procedure_id : 1260144744799178847
     type_code : 27
     code : |1+
      procedure Constructor()
      
     type : 589824
   -
     name : Destructor
     procedure_id : 1260144744799244383
     type_code : 28
     code : |1-
      procedure Destructor()
     type : 655360
   -
     name : set_up
     procedure_id : 1260148296769744256
     type_code : 12
     code : |1+
      procedure set_up()
      :uuid_generator <- cUuidGeneratorForTest.create()
      :activity_repository <- cActivityRepositoryForTest.create()
      :activity_api <- cActivityApi.create(<activity_repository>: :activity_repository, <uuid_generator>: uuid_generator)
     type : 458752
   -
     name : should_add_activity_to_repository_when_add_activity
     procedure_id : 1260144744799309919
     type_code : 12
     code : |1+
      procedure should_add_activity_to_repository_when_add_activity()
      
      sub_test("add activity with date '2010.09.25'", () => {...
      	add_activity(<activity_uuid>: "a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1", <activity_date>: "20100925")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal("20100925", activity.get_activity_date().to_date())
      })
      
      sub_test("add activity with date '2012.07.13'", () => {...
      	add_activity(<activity_date>: "20120713")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal("20120713", activity.get_activity_date().to_date())
      })
      
      sub_test("add activity with start = '14:17' and end = '17:25'", () => {...
      	add_activity(<activity_start>: "141700000", <activity_end>: "172500000")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal("141700000", activity.get_activity_period().to_start_time())	
      	assert_equal("172500000", activity.get_activity_period().to_end_time())	
      })
      
      sub_test("add activity with start = '23:25' and end = '23:50'", () => {...
      	add_activity(<activity_start>: "232500000", <activity_end>: "235000000")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal("232500000", activity.get_activity_period().to_start_time())	
      	assert_equal("235000000", activity.get_activity_period().to_end_time())	
      })
      
      
      sub_test("add activity with description = 'some description'", () => {...
      	add_activity(<activity_description>: "some description")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal(<expected>: "some description", <obtained>: activity.get_activity_description().to_string())
      })
      
      sub_test("add activity with customer = 'my lovely customer'", () => {...
      	add_activity(<customer_name>: "my lovely customer")
      	activity is dtoActivity dynamic <- :activity_repository.get_activity_by_uuid("a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	assert_equal(<expected>: "my lovely customer", <obtained>: activity.get_customer_name().to_string())
      })
     type : 458752
   -
     name : add_activity
     procedure_id : 1260148176510632515
     type_code : 12
     code : |1+
      procedure add_activity(activity_uuid is uuid = "a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1", activity_date is date = "20201005", activity_start is time = "0900000", activity_end is time = "1000000", activity_description is string = "...", customer_name is string = "some customer")
      :uuid_generator.initialize_next_uuid(<next_uuid>: activity_uuid)
      activity_api.add_activity(<activity_date>: tyActivityDate.create(activity_date), <activity_period>: tyActivityPeriod.create(activity_start, activity_end), <customer_name>: tyCustomerName.create(customer_name), <activity_description>: tyActivityDescription.create(activity_description))
      
     type : 458752
   -
     name : should_generate_uuid_for_activity_when_add_activity
     procedure_id : 1260274452688822450
     type_code : 12
     code : |1+
      procedure should_generate_uuid_for_activity_when_add_activity()
      sub_test("with uuid 'a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1'", () => {
      	:add_activity(<activity_uuid>: "a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      	:assert_activities_uuid_in_repository(<expected>: "a0409aac-3b8a-4af2-87ca-9d5da2f1ffe1")
      })
      
      sub_test("with uuid 'a027be93-1b9a-48e8-b77f-6f651a6b89ea'", () => {
      	:add_activity(<activity_uuid>: "a027be93-1b9a-48e8-b77f-6f651a6b89ea")
      	:assert_activities_uuid_in_repository(<expected>: "a027be93-1b9a-48e8-b77f-6f651a6b89ea")
      })
     type : 458752
   -
     name : assert_activities_uuid_in_repository
     procedure_id : 1260275839963517100
     type_code : 12
     code : |1+
      procedure assert_activities_uuid_in_repository(expected is string)
      activities is array of dtoActivity dynamic <- :activity_repository.get_all_activities()
      actual is array of string = activities.map(activity => activity.get_activity_uuid().to_uuid())
      assert_equal(expected, actual.ToString(", "))
      
     type : 458752
  procedure_templates : []
  property_templates : []
 code_parameters :
  internal_properties : BgAAAAYAAAA6ih3UbgNXHwTtiPSFUEj+2fi/m7v4QV2rqidAupM=
  original_name : Classe1
resources :
 string_res :
  identifier : 0x117ceeb7074c919d
  internal_properties : BgAAAAYAAAAnMYFQ1bL/vz9ehh7L22SNNSlIzGTOI8h5F/WtgDNP
custom_note :
 internal_properties : BgAAAAYAAABtB9HWVzrXO2+4NDRVK0vmzaNKrCKqH1DBX30lMmGZ
